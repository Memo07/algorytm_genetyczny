import com.sun.deploy.util.ArrayUtil;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class AlgorytmTest {

    private static int [][] odleglosci=new int[5][];
    private static int [][] populacja=new int[2][];
    private static int [] osobnik=new int[]{0,1,2,3,4,5,6,7,8,9};

    @Before
    public void prepareTest()throws IOException{
        odleglosci[0]=new int[]{0,3,3,1,2};
        odleglosci[1]=new int[]{3,0,3,5,1};
        odleglosci[2]=new int[]{3,3,0,4,4};
        odleglosci[3]=new int[]{1,5,4,0,7};
        odleglosci[4]=new int[]{2,1,4,7,0};
        populacja[0]=new int[]{2,1,0};
        populacja[1]=new int[]{3,2,4};

    }

    @Test
    public void tworzOceneTest(){
        assertArrayEquals(new int[]{9,15},Algorytm.tworzOcene(populacja));
    }

    @Test
    public void isPerfectPowerTest(){
        assertArrayEquals(new int[]{2,2},Alg.isPerfectPower(4));
        assertNull(Alg.isPerfectPower(5));
        assertArrayEquals(new int[]{3,2},Alg.isPerfectPower(9));
        assertArrayEquals(new int[]{5,3},Alg.isPerfectPower(125));
        assertArrayEquals(new int[]{200,3},Alg.isPerfectPower(200));
    }

    @Test
    public void inverseMutationTest(){
        Algorytm.inverseMutation(osobnik);
        assertArrayEquals(new int[]{0,1,4,3,2,5,6,7,8,9},osobnik);

    }

}