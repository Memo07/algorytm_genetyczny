
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;



public class Alg {
    static int[][] loadTable() throws IOException {
        BufferedReader input = new BufferedReader(new FileReader("C:\\Users\\Marcin\\Documents\\ALgorytmyGenetyczne\\src\\berlin52.txt"));
        int[][] table = new int[52][52];
        for (int i = 0; i < table.length; i++) {
            String[] line = input.readLine().trim().split(" ");
            int[] temp = Arrays.stream(line).mapToInt(Integer::parseInt).toArray();
            for (int j = 0; j < temp.length; j++) {
                table[i][j] = temp[j];
                table[j][i] = table[i][j];
            }
        }
        return table;
    }

    static void printTable(int[][] table) {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static int[] isPerfectPower(int n){
        int [] result;
        double tmp;
        for(int i=2;i<=100;i++){
            tmp=Math.pow(n,1.0/i);
            if(tmp%1.0>0.99||tmp%1.0<0.01){
                result=new int[]{(int)tmp,i};
                return result;
            }
        }
        return null;
    }

    public static void main(String[] args) throws IOException {
        int[][] table = Alg.loadTable();
        printTable(table);
        //Alg.isPerfectPower(125);
    }
}