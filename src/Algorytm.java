import com.sun.istack.internal.NotNull;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Algorytm {
    private static int maxSwapMutation;
    private static Random random=new Random();
    private static int[][] tablicaOdleglosci;
    private static int count=0;
    static{
        try {
             tablicaOdleglosci= Alg.loadTable();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    @NotNull
    private static int [][] generujPokolenie(int [][] populacja){
        int [] oceny=tworzOcene(populacja);
        int [][] pokolenie=new int[populacja.length][tablicaOdleglosci.length];
        int [][] skrzyzowane;
        for(int i=0;i<populacja.length;i++){
            pokolenie[i]=Arrays.copyOf(populacja[selekcja(oceny)],tablicaOdleglosci.length);
        }
        for(int i=0;i<pokolenie.length-1;i+=2){
            if(random.nextDouble()<0.90) {
                skrzyzowane = crossoverPMX(pokolenie[i], pokolenie[i + 1]);
                pokolenie[i] = skrzyzowane[0];
                pokolenie[i + 1] = skrzyzowane[1];
            }
        }
        for(int [] osobnik:pokolenie) {
            if (random.nextDouble() < 0.005) {
                if(random.nextDouble()<0.8) {
                    for (int temp = random.nextInt(maxSwapMutation+1) + 1; temp > 0; temp--) {
                        swapMutation(osobnik);
                    }
                }
                else{
                    inverseMutation(osobnik);
                }
            }
        }
        return pokolenie;
    }

    private static int [][] generujPopulacje(int k,int n){
        int[][] populacja=new int[k][n];
        for(int i=0;i<populacja.length;i++) {
            Arrays.fill(populacja[i],-1);
            for (int j = 0; j < populacja[i].length; j++) {
                int r;
                boolean flag;
                do {
                    r = random.nextInt(n);
                    flag=false;
                    for (int z = 0; z <= j && !flag; z++) {
                        flag = Integer.valueOf(populacja[i][z]).equals(r);
                    }
                } while (flag);
                populacja[i][j] = r;
            }
        }
        return populacja;
    }

    static int [] tworzOcene(int [][] populacja) {
       int [] ocena=new int[populacja.length];
            for(int osobnik=0;osobnik<populacja.length;osobnik++){
                int sum=0;
                for(int miasto=1;miasto<populacja[osobnik].length;miasto++) {
                    sum+=tablicaOdleglosci[populacja[osobnik][miasto]][populacja[osobnik][miasto-1]];
                }
                sum+=tablicaOdleglosci[populacja[osobnik][0]][populacja[osobnik][populacja[osobnik].length-1]];
                ocena[osobnik]=sum;
            }
       return ocena;
   }
   private static int selekcja(int [] oceny){
       int index=random.nextInt(oceny.length);
       int wybor=oceny[index];
       int wynik=index;
       for(int i=0;i<1;i++){
            index=random.nextInt(oceny.length);
            if(oceny[index]<wybor){
                wybor=oceny[index];
                wynik=index;//jeżeli losowane rozwiązanie jest krótsze to podstawiamy jako nasz wybór
            }
        }
       return wynik;
   }

   private static int selekcjaRuletka(int [] oceny){
        int maxOcena=Arrays.stream(oceny).max().isPresent()?Arrays.stream(oceny).max().getAsInt():0; //-implementacja mx z tablicy dla przyszlej selekcji kola ruletki
        double rand=random.nextDouble();
        oceny=Arrays.stream(oceny).sorted().toArray();
        double [] ocenyOdwrocone=new double[oceny.length];
        for(int i=0;i<ocenyOdwrocone.length;i++){
            ocenyOdwrocone[i]=(maxOcena-oceny[i]);
        }
        double suma=Arrays.stream(ocenyOdwrocone).sum();
       for(int i=0;i<ocenyOdwrocone.length;i++){
            if(ocenyOdwrocone[i]/suma<rand) {
                return i;
            }

        }
        return 0;
   }


    @NotNull
    private static int [][] crossoverPMX(int [] tab1, int [] tab2){
        int startIndex=random.nextInt(tab1.length-1)+1;
       int endIndex=random.nextInt(tab1.length-startIndex)+startIndex;
       int [] result1=new int[tab1.length];
       Arrays.fill(result1,-1);
       int [] result2=new int[tab2.length];
       Arrays.fill(result2,-1);
       for(int i=startIndex;i<=endIndex;i++){
           result1[i]=tab2[i];
           result2[i]=tab1[i];
       }

        wypelnij(result1,tab1,startIndex,endIndex);
        wypelnij(result2,tab2,startIndex,endIndex);
        return new int[][]{result1,result2};
    }

    private static void wypelnij(int [] result1, int [] tab1, int start, int end){
        String res1Str="*"+Arrays.stream(result1).boxed().map(Object::toString).collect(Collectors.joining("*"))+"*";
        boolean found=false;
        for(int i=0;i<result1.length;i++){
            if(i>=start&&i<=end){
                continue;
            }
            if(!found) {
                result1[i] = tab1[i];
            }
            if(res1Str.contains("*"+result1[i]+"*")){
                for(int j=start;j<=end;j++){
                    if(result1[i]==result1[j]){
                        result1[i]=tab1[j];
                        found=true;
                        i--;
                        break;
                    }
                }
            }else{
                found=false;
            }
        }
    }

    private static void swapMutation(int [] osobnik){
        int pos1,pos2;
        do {
            pos1 = random.nextInt(osobnik.length);
            pos2 = random.nextInt(osobnik.length);
        }while(pos1==pos2);
        int temp=osobnik[pos1];
        osobnik[pos1]=osobnik[pos2];
        osobnik[pos2]=temp;
    }

    static void inverseMutation(int [] osobnik){
        int startIndex=random.nextInt(osobnik.length-1);
        int endIndex=random.nextInt(osobnik.length-startIndex)+startIndex;
        int [] inverse=Arrays.copyOfRange(osobnik,startIndex,endIndex);
        inverse=Arrays.stream(inverse).boxed().sorted(Comparator.reverseOrder()).mapToInt(Integer::intValue).toArray();
        System.arraycopy(inverse,0,osobnik,startIndex,inverse.length);
        count++;
    }

    private static int pokazDlugoscTrasy(int [][] wynik,boolean showRoute){
        int min=Integer.MAX_VALUE;
        int sum;
        int index=0;
        for(int osobnik=0;osobnik<wynik.length;osobnik++){
            sum=0;
            for(int miasto=1;miasto<wynik[osobnik].length;miasto++) {
                sum+=tablicaOdleglosci[wynik[osobnik][miasto]][wynik[osobnik][miasto-1]];
            }
            sum+=tablicaOdleglosci[wynik[osobnik][0]][wynik[osobnik][wynik[osobnik].length-1]];
            if(sum<min){
                min=sum;
                index=osobnik;
            }
        }
        if(showRoute) {
            System.out.println(Arrays.stream(wynik[index]).boxed().map(Object::toString).collect(Collectors.joining("-")));
        }
        return min;
    }


    public static void main(String [] args){
        long time=System.currentTimeMillis();
        maxSwapMutation=4;
        int [][] populacja=generujPopulacje(500,tablicaOdleglosci.length);
        int pokolenie=1;
           do{
               populacja=generujPokolenie(populacja);
               System.out.printf("Wygenerowano pokolenie numer: %d, czas: %.2f`s dlugośćTrasy: %d %d\n",pokolenie,((System.currentTimeMillis()-time)/1000.0),pokazDlugoscTrasy(populacja,false),count);
               pokolenie++;
           }while (pokolenie<=2*10e4);//(System.currentTimeMillis()-time)<(2*60*10e2));
        System.out.printf(pokazDlugoscTrasy(populacja,true) + "\n %.2f`s", (System.currentTimeMillis() - time) / 1000.0);
        // wypelnij(new int[]{0,0,4,8,6,0},new int[]{2,4,8,7,2,5},2,4);

        //int [] test=wypelnij(new int[]{0,0,0,2,3,4,0,0,0},new int[]{1,5,2,7,8,9,10,11,12},3,5);
    }
}
